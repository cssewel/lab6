/******************* 
  Caleb Sewell 
  cssewel               
  Lab 05                           
  Lab Section: 001
  Nushrat Humaira            
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card 
{
	Suit suit;
	int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) 
{
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
	int countS = 0;
	int countH = 0;
	int countD = 0;
	int countC = 0;
	
	srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
	Card cardArr[52];

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	
	for (int i = 0; i < 52; i++)
	{
		if (i < 13)
		{
			countS++;
			cardArr[i].suit = SPADES;
			cardArr[i].value = countS;		
		}
		
		else if (i >= 13 && i < 26)
		{
			countH++;
			cardArr[i].suit = HEARTS;
			cardArr[i].value = countH;
		}
		
		else if (i >= 26 && i < 39)
		{
			countD++;
			cardArr[i].suit = DIAMONDS;
			cardArr[i].value = countD;
		}
		
		else
		{
			countC++;
			cardArr[i].suit = CLUBS;
			cardArr[i].value = countC;
		}
	}

	random_shuffle(&cardArr[0], &cardArr[51], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
	Card handOfCards[5] = { cardArr[0], cardArr[1], cardArr[2], cardArr[3], cardArr[4] };

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
	sort(&handOfCards[0], &handOfCards[4], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
	cout << "\n";
	for (int i = 0; i < 5; i++)
	{
		cout << setw(8) << get_card_name(handOfCards[i]) << " of " << get_suit_code(handOfCards[i]) << "\n";
	}
	cout << "\n";

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
	if (lhs.suit < rhs.suit)
	{
		return true;
	}
	return 1;
}

string get_suit_code(Card& c) 
{
  switch (c.suit) 
  {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "Something went wrong :(";
  }
}

string get_card_name(Card& c) 
{
  // IMPLEMENT
	switch (c.value)
	{
		case 1: 	return "2";
		case 2: 	return "3";
		case 3: 	return "4";
		case 4: 	return "5";
		case 5: 	return "6";
		case 6: 	return "7";
		case 7: 	return "8";
		case 8: 	return "9";
		case 9:	 	return "10";
		case 10: 	return "Jack";
		case 11: 	return "Queen";
		case 12:	return "King";
		case 13: 	return "Ace";
		default:	return "Something went wrong :(";
	}
}
